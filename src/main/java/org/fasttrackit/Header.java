package org.fasttrackit;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;

public class Header {
    private SelenideElement loginButton = $("[data-icon=sign-in-alt]");
    private SelenideElement greetingsElement
            = $(".navbar-text span span");
    private final SelenideElement basketIcon = $(".fa-shopping-cart");
    private SelenideElement cartBadge= $(".shopping_cart_badge");


    public Header() {
    }
    public String getNumberOfProductInCart(){
        return this.cartBadge.text ();
    }

    public void clickOnTheLoginButton() {

        System.out.println ("Clicked on the " + loginButton);
        loginButton.click ();
    }
    public String getGreetingsMsg() {
        return greetingsElement.text ();
    }
    public void clickOnTheCartIcon(){
        basketIcon.click ();
    }
}
