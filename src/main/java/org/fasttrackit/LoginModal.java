package org.fasttrackit;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;

public class LoginModal {
    private SelenideElement userField = $("#user-name");
    private SelenideElement passwordField = $("#password");
    private SelenideElement submitButton = $("[type=submit]");
    public void fillInUsername(String user){
        userField.sendKeys (user);
    }
    public void fillInPassword(String password){
        passwordField.sendKeys (password);
    }
    public void clickSubmitButton(){
        submitButton.click ();
        System.out.println ("Clicked on the " + submitButton );
    }
}
