package org.fasttrackit.dataprovider;

public class User {

    private final String userName;
    private final String password;
    private final String expectedGreetingsMsg;

    public User(String userName, String password) {
        this.userName=userName;
        this.password=password;
        this.expectedGreetingsMsg= "Hi " + userName + "!";
    }

    public String getUserName() {
        return userName;
    }

    public String getPassword() {
        return password;
    }

    public String getExpectedGreetingsMsg() {
        return expectedGreetingsMsg;
    }

    @Override
    public String toString(){
        return "Test run with: " + userName;

    }
}
