package org.fasttrackit.dataprovider;

import org.testng.annotations.DataProvider;

public class UserDataProvider {
    @DataProvider(name = "validUserDataProvider")
    public Object[][] feedUserDataProvider(){
        User beetleUser = new User ("beetle", "choochoo");
        User dinoUser = new User ("dino", "choochoo");
        User turtleUser = new User ("turtle", "choochoo");

        return new Object[][] {
                {beetleUser},
                {dinoUser},
                {turtleUser}
        };
    }

}
