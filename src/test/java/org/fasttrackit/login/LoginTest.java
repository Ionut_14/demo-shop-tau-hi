package org.fasttrackit.login;

import io.qameta.allure.Feature;
import org.fasttrackit.DemoShopPage;
import org.fasttrackit.LoginModal;
import org.fasttrackit.config.BaseTestConfig;
import org.fasttrackit.dataprovider.User;
import org.fasttrackit.dataprovider.UserDataProvider;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import org.w3c.dom.UserDataHandler;

import static org.testng.Assert.*;

@Feature ("User Login")
public class LoginTest extends BaseTestConfig {
    DemoShopPage page;

    @BeforeTest
    public void setup(){
        page = new DemoShopPage ();
        page.openDemoShopApp ();
    }

    @AfterMethod
    public void reset(){
        page.getFooter ().resetPage ();
    }

    @Test(dataProviderClass = UserDataProvider.class, dataProvider = "validUserDataProvider")
    public void userCanLoginDemoShopPage(User user){
        page.getHeader ().clickOnTheLoginButton ();
        LoginModal loginModal = new LoginModal ();
        loginModal.fillInUsername (user .getUserName ());
        loginModal.fillInPassword (user .getPassword ());
        loginModal.clickSubmitButton ();
        assertEquals (page.getHeader ().getGreetingsMsg (),user .getExpectedGreetingsMsg (), " Greetings message is: Hi user !");
    }

}
